const Modal=(()=>{
  let player1=document.getElementById('player1');
  let player2=document.getElementById('player2');
  const playerValidation=()=>{
    if(player1.value==""||player2.value==""){
      alert("Please input some player names");
    }
    else{
      hide();
    }
  }
  const hide=()=>{
    document.getElementById('Modal').style.display="none";
    document.getElementById('content-wrapper').style.display="flex";
    clearFields();
  }
  const startEvent=()=>{
    let startBtn=document.getElementById('startBtn');
    startBtn.addEventListener('click',function(){
      playerValidation();
    });
  }
  const clearFields=()=>{
    player1.value="";
    player2.value="";
  }
  return startEvent();
})();

const Game=(()=>{
  let btns=document.querySelectorAll('.choiceBox');
  let arrayFromBtns=Array.from(btns);
  const GameBoard=()=>{
    let board=["","","","","","","","",""];
    let changeArray=(index,currentPlayer)=>board[index]=currentPlayer;
    //resets the board
    let resetBoard=()=>{
      board.map(function(x,i,arr){
        arr[i]="";
      });
    };
    return {board,changeArray,resetBoard}
  }
  let gameBoard=GameBoard();
  const Player=(name,symbol)=>{
    const makeMove=element=>{
      element.value=symbol;
    }
    return {name,makeMove,symbol}
  }
  const BtnObj={
    disableAllBtns:()=>{
      arrayFromBtns.forEach(btn=>{
        btn.disabled=true;
      });
    },
    enableAllBtns:()=>{
      arrayFromBtns.forEach(btn=>{
        btn.disabled=false;
      });
    },
    clearBtnsValue:()=>{
      arrayFromBtns.forEach(btn=>{
        btn.value="";
      });
    },
    disableBtn:(element)=>{
      element.disabled=true;
    }
  }
  const checkTurn=(turn)=>{
    if(turn){
      return true;
    }
    return false;
  }
  const startNewGame=(newGameBtn)=>{
    BtnObj.enableAllBtns();
    BtnObj.clearBtnsValue();
    newGameBtn.parentNode.removeChild(newGameBtn);
    document.getElementById('content-wrapper').style.display="none";
    document.getElementById('Modal').style.display="flex";
  }
  const addNewGameBtn=()=>{
    let wrapper=document.querySelector('#content-wrapper');
    let newGameBtn=document.createElement('INPUT');
    newGameBtn.setAttribute('type','button');
    newGameBtn.setAttribute('value','New Game');
    newGameBtn.style.width="15%";
    newGameBtn.style.height="5%";
    newGameBtn.style.fontSize="2em";
    wrapper.insertBefore(newGameBtn,wrapper.firstChild);
    newGameBtn.addEventListener('click',function(){
      gameBoard.resetBoard();
      startNewGame(newGameBtn);
    });
  }
  let player1=Player(document.getElementById('player1').value,'x');
  let player2=Player(document.getElementById('player2').value,'o');
  let result="";
  const checkWin=(gameBoardArray,playerName)=>{
    let winConditions=[
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6]
    ];
    const isTripleX=currentValue=>{
      return currentValue==="x";
    }
    const isTripleO=currentValue=>{
      return currentValue==="o";
    }
    const isTie=currentValue=>{
      return currentValue!="";
    }
    for (var i = 0; i < winConditions.length; i++) {
      //loops through winConditions to check every single Array
      // and compare the current value to see if it matches the pattern
      let currentCheck=[];
      let currentWinCondition=winConditions[i];
      currentCheck.push(gameBoardArray[currentWinCondition[0]],
        gameBoardArray[currentWinCondition[1]],
        gameBoardArray[currentWinCondition[2]]);
      console.log(currentCheck);
      // console.log(currentCheck.every(isTripleX));
      if(currentCheck.every(isTripleX)){
        result="X win";
        alert('X wins');
        BtnObj.disableAllBtns();
        addNewGameBtn();
        break;
      }
      if(currentCheck.every(isTripleO)){
        result="O win";
        alert('O wins');
        BtnObj.disableAllBtns();
        addNewGameBtn();
        break;
      }
      if(gameBoardArray.every(isTie)){
        result="Tie";
        alert('Tie');
        BtnObj.disableAllBtns();
        addNewGameBtn();
        break;
      }
    }
  }
  const Controller=()=>{
    const listenForBoxes=()=>{
      let boxesNode=document.querySelectorAll('.choiceBox');
      let boxesArr=Array.from(boxesNode);
      let playerTurn=true;
      boxesArr.forEach(function(element,index){
        element.addEventListener('click',function(){
          showResult(playerTurn,boxesArr,element,index);
          playerTurn=!playerTurn;
        });
      });
    };
    listenForBoxes();
  };
  const showResult=(playerTurn,boxesArr,element,index)=>{
    if(checkTurn(playerTurn)){
      console.log(player1.symbol);
      BtnObj.disableBtn(element);
      player1.makeMove(element);
      gameBoard.changeArray(index,player1.symbol);
      checkWin(gameBoard.board,player1.name);
    }
    else{
      console.log(player2.symbol);
      BtnObj.disableBtn(element);
      player2.makeMove(element);
      gameBoard.changeArray(index,player2.symbol);
      checkWin(gameBoard.board,player1.name);
    }
  };
  Controller();
})();
